FROM phusion/baseimage:0.10.0


# Set the locale
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update && \
    apt-get install -y \
        build-essential \
        bzip2 \
        firefox \
        git \
        libcanberra-gtk3-module \
        libxext-dev \
        libxrender-dev \
        libxss1 \
        libxtst-dev \
        openjdk-9-jre \
        sudo \
        tmux \
        xvfb \
        wget \
        zenity

# install firefox and driver
RUN apt-get install -y firefox
RUN wget -q https://github.com/mozilla/geckodriver/releases/download/v0.13.0/geckodriver-v0.13.0-linux64.tar.gz
RUN gzip -dc geckodriver-v0.13.0-linux64.tar.gz | tar xvf -
RUN mv geckodriver /usr/local/bin/
RUN chmod +x /usr/local/bin/geckodriver
RUN rm geckodriver-v0.13.0-linux64.tar.gz

#RUN apt clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*



ARG MY_UID
ARG MY_GID

RUN echo "developer:x:$MY_UID:$MY_GID:Developer,,,:/home/developer:/bin/bash"
RUN echo "developer:x:$MY_GID:"

RUN mkdir -p /home/developer && \
    echo "developer:x:$MY_UID:$MY_GID:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:$MY_GID:" >> /etc/group && \
    mkdir -p /etc/sudoers.d && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown developer:developer -R /home/developer


# Install miniconda
ENV PATH /miniconda/bin:${PATH}
RUN curl -LO https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    bash Miniconda3-latest-Linux-x86_64.sh -p /miniconda -b && \
    rm Miniconda3-latest-Linux-x86_64.sh


USER developer
ENV HOME /home/developer
WORKDIR /home/developer
env PATH /home/developer/bin:$PATH

ARG pycharm_version=community-2017.3.3

RUN curl -LO https://download.jetbrains.com/python/pycharm-${pycharm_version}.tar.gz && \
    mkdir ~/local && \
    tar xzf pycharm-${pycharm_version}.tar.gz -C ~/local && \
    rm pycharm-${pycharm_version}.tar.gz

RUN mkdir /home/developer/bin && \
    ln -s /home/developer/local/pycharm-${pycharm_version}/bin/pycharm.sh /home/developer/bin/pycharm.sh


# private packages
# COPY docker/ide/.pip/pip.conf /root/.pip/

COPY docker/ide/environment.yml /tmp/environment.yml
RUN /miniconda/bin/conda config --add channels conda-forge
RUN /miniconda/bin/conda env create --force -f /tmp/environment.yml


ENV PATH /home/developer/.conda/envs/ide/bin:$PATH
ENV PYTHONPATH .

ENTRYPOINT ["/home/developer/bin/pycharm.sh"]
